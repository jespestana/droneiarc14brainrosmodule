#include "iarc14_dronearchitecturebrain.h"

DroneArchitectureBrain::DroneArchitectureBrain() :
    DroneModule( droneModule::active, FREQ_ARCHITECTURE_BRAIN),
    this_drone_interface(),
    brain_state_machine( &this_drone_interface )
    {
    init();
    return;
}

DroneArchitectureBrain::~DroneArchitectureBrain() {
    close();
    return;
}

void DroneArchitectureBrain::init() {
    //end
    DroneModule::init();
    return;
}

void DroneArchitectureBrain::open(ros::NodeHandle & nIn, std::string moduleName) {
    //Node
    DroneModule::open(nIn,moduleName);

    this_drone_interface.open(n);

    isInTheSystemPubl    = n.advertise<std_msgs::Bool>( DRONE_ARCHITECTURE_BRAIN_IS_IN_THE_SYSTEM, 1, true);
    brain_state_machine.open(n);

    //Flag of module opened
    droneModuleOpened=true;

    return;
}

void DroneArchitectureBrain::close() {
    DroneModule::close();
    return;
}

bool DroneArchitectureBrain::run() {
    DroneModule::run();

    // publish this_drone's id number
    {
    std_msgs::Bool this_drone_is_in_the_system;
    this_drone_is_in_the_system.data = brain_state_machine.getIsInTheSystemBool();
    isInTheSystemPubl.publish(this_drone_is_in_the_system);
    }

#ifdef TEST_WITH_AUTONOMOUS_BRAIN
//    std::cout << "DroneArchitectureBrain::run() if (isStarted()), isStarted:" << isStarted() << std::endl;
    if (isStarted()) {
        bool everything_ok = brain_state_machine.run();
//        if (!everything_ok) {
//            stop();
//        }
    }
#endif // TEST_WITH_AUTONOMOUS_BRAIN

//    std::cout << "LEAVING DroneArchitectureBrain::run()" << isStarted() << std::endl;
    return true;
}
